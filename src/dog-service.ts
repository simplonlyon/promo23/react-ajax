import axios from "axios";
import { Dog } from "./entities";

/**
 * On crée ici les différentes fonctions qui permettront de faire les requêtes HTTP vers l'API Rest en
 * Symfony. On aura généralement les fonctions correspondantes à ce qu'on a définit dans le contrôleur
 * de l'entité ciblée
 */

export async function fetchAllDogs() {
    const response = await axios.get<Dog[]>('http://localhost:8000/api/dog');
    return response.data;
}

export async function fetchOneDog(id:number) {
    const response = await axios.get<Dog>('http://localhost:8000/api/dog/'+id);
    return response.data;
}

export async function postDog(dog:Dog) {
    const response = await axios.post<Dog>('http://localhost:8000/api/dog', dog);
    return response.data;
}

export async function updateDog(dog:Dog) {
    const response = await axios.put<Dog>('http://localhost:8000/api/dog/'+dog.id, dog);
    return response.data;
}

export async function deleteDog(id:any) {
    await axios.delete('http://localhost:8000/api/dog/'+id);
}