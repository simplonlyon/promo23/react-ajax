import ItemDog from "@/components/ItemDog";
import { fetchAllDogs } from "@/dog-service";
import { Dog } from "@/entities";
import { useEffect, useState } from "react";



export default function Index() {

    const [dogs, setDogs] = useState<Dog[]>([]);
    /**
     * Le useEffect est un "hook" de react qui sert à relancer ce qu'il contient lorsque certaines 
     * valeurs changent. Il prend en premier argument la fonction à relancer et en deuxième argument 
     * un tableau dans lequel on va lister les valeurs "à surveiller". Ici on donne un tableau vide
     * ce qui fait que cette fonction sera lancée une fois à l'initialisation du component et c'est tout
     * (c'est un pattern assez courant, car on a souvent besoin de déclencher certaines choses à l'affichage
     * d'un component)
     */
    useEffect(() => {
        //On lance notre fetchAll pour aller chercher les chiens sur le serveur quand on arrive sur la page
        fetchAllDogs().then(data => {
            setDogs(data);
        });

    }, []);

    return (
        <>
            <h1>List of dogs</h1>
            {dogs.map(item =>
                <ItemDog key={item.id} dog={item} />
            )}
        </>
    );
}