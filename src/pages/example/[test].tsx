import PropEvent from "@/components/PropEvent";
import { useRouter } from "next/router";


/**
 * Avec le routeur de NextJs, les routes/pages accessibles sont définies par les
 * noms des fichiers et dossiers contenues dans le dossier pages.
 * On peut créer des routes dynamique en nommant nos fichier avec des [],
 * ici notre fichier se trouve dans le dossier example et s'appel [test].tsx,
 * cela signifie qu'on tombera sur ce fichier pour n'importe quelle route commençant
 * par /example/... (http://localhost:3000/example/valeur, http://localhost:3000/example/autre,
 * http://localhost:3000/example/etc)
 * La valeur mise dans l'url pourra être récupérer avec le router (qu'on récupère
 * ici avec useRouter). Le nom des paramètres de route correspondront au nom du fichier
 * entre crochets, ici test
 */
export default function Test() {

    const router = useRouter();
    const {test} = router.query;

    return (
        <>
        <h1>Test Page :  {test}</h1>

        <PropEvent onAction={() => alert('coucou')} />
        <PropEvent onAction={() => console.log('au revoir')} />
        </>
    );
}