import FormDog from "@/components/FormDog";
import { deleteDog, fetchOneDog, updateDog } from "@/dog-service";
import { Dog } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

export default function DogPage() {
    const router = useRouter();
    const { id } = router.query;
    const [dog, setDog] = useState<Dog>();
    const [showEdit, setShowEdit] = useState(false);
    /**
     * Notre route dynamique va récupérer l'id ici, on s'en sert dans un
     * useEffect pour lancer une requête vers l'API rest pour récupérer le
     * chien correspondant à l'id en question.
     */
    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneDog(Number(id))
            .then(data => setDog(data))
            .catch(error => {
                //Si on a une erreur 404 on redirige vers la page 404 par défaut
                //Pas idéal, mais on fera un truc mieux quand on fera du SSR avec next
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404');
                }
            });


    }, [id]);

    async function remove() {
        await deleteDog(id);
        router.push('/');
    }
    async function update(dog:Dog) {
        const updated = await updateDog(dog);
        setDog(updated);
    }

    function toggle() {
        setShowEdit(!showEdit);
    }

    if (!dog) {
        return <p>Loading...</p>
    }

    return (
        <>
            <h1>Dog {dog.name}</h1>
            <p>Breed : {dog.breed}</p>

            <button onClick={remove}>Delete</button>
            <button onClick={toggle}>Edit</button>

            {showEdit &&
                <>
                    <h2>Edit dog</h2>
                    <FormDog edited={dog} onSubmit={update} />
                </>
            }
        </>
    );
}