import { Dog } from "@/entities";
import Link from "next/link";

/**
 * En général on définit les props directement au dessus du component qui les utilise car il n'y
 * a pas d'autres endroits où on s'en servira
 */
interface Props {
    dog: Dog;
}
/**
 * On destructure nos props dans les argument pour récupérer celles qu'on a besoin, alternativement on
 * pourrait aussi mettre (props:Props) mais alors dans le component il faudrait répéter props partout genre
 * props.dog.name, props.dog.breed etc.
 */
export default function ItemDog({ dog }: Props) {


    return (
        <article>
            <h3>{dog.name}</h3>
            <p>breed : {dog.breed}</p>
            {dog.birthdate &&
                <p>birthdate : {new Date(dog.birthdate).toLocaleDateString()}</p>
            }
            <Link href={"/dog/"+dog.id}>See More</Link>
        </article>
    );

}

